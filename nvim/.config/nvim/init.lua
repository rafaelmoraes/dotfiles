require "user.impatient"
require "user.options"
require "user.keymaps"

require "user.plugins"
require "user.colorscheme"
require "user.autocomplete"
require "user.lsp"
require "user.fuzzy_finder"

require "user.syntax_highlight"
require "user.autopairs"
require "user.comment"
require "user.indentline"
require "user.which_key"

require "user.gitsigns"

require "user.file_explorer"

require "user.lualine"
require "user.toggleterm"
