local colorscheme = "solarized8_flat"
-- local colorscheme = "dracula"
-- local background = "dark"
local background = "light"

vim.cmd("set background=" .. background)

local ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)

if not ok then
  vim.notify("Error: colorscheme " .. colorscheme .. " not found!")
  return
end
