local ok, impatient = pcall(require, 'impatient')
if not ok then
  vim.notify "Error: plugin 'impatient' not found!"
  return
end

impatient.enable_profile()
