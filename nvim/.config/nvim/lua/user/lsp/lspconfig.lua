local ok, _ = pcall(require, "lspconfig")
if not ok then
  vim.notify("Error: plugin 'lspconfig' not found!")
  return
end

local ok, mason_lspconfig = pcall(require, "mason-lspconfig")
if not ok then
  vim.notify "Error: plugin 'mason-lspconfig' not found!"
  return
end

mason_lspconfig.setup({
  automatic_installation = true,
})

mason_lspconfig.setup_handlers {

  -- The first entry (without a key) will be the default handler
  -- and will be called for each installed server that doesn't have
  -- a dedicated handler.
  function (server_name) -- default handler (optional)
    require("lspconfig")[server_name].setup {}
  end,
  -- Next, you can provide a dedicated handler for specific servers.
  -- For example, a handler override for the `rust_analyzer`:
  -- ["rust_analyzer"] = function ()
  --   require("rust-tools").setup {}
  -- end
}
