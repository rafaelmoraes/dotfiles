local ok, mason = pcall(require, "mason")
if not ok then
  vim.notify "Error: plugin 'mason' not found!"
  return
end

mason.setup()

