local ok, null_ls = pcall(require, "null-ls")
if not ok then
  vim.notify "Error: plugin 'null-ls' not found!"
  return
end

local ok, mason_null_ls = pcall(require, "mason-null-ls")
if not ok then
  vim.notify "Error: plugin 'mason-null-ls' not found!"
  return
end

mason_null_ls.setup({
    ensure_installed = nil,
    automatic_installation = true,
    automatic_setup = true,
})

null_ls.setup()
mason_null_ls.setup_handlers()

