-- Auto Install
local fn = vim.fn
local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
  packer_bootstrap = fn.system({ 'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path })
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]]

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
  return
end

-- Have packer use a popup window
packer.init {
  display = {
    open_fn = function()
      return require("packer.util").float { border = "rounded" }
    end,
  },
}

-- Install your plugins here
return packer.startup(function(use)
  use 'wbthomason/packer.nvim'
  use "lewis6991/impatient.nvim" --Improve startup time
  use "nvim-lua/popup.nvim" -- An implementation of the Popup API from vim in Neovim
  use "nvim-lua/plenary.nvim" -- Useful lua functions used by lots of plugins

  --Writer helpers
  use "windwp/nvim-autopairs" -- Autopairs, integrates with both cmp and treesitter
  use "numToStr/Comment.nvim" -- Easily comment stuff
  use "folke/which-key.nvim" -- Show key bindings
  -- use 'rmagatti/auto-session' -- Session Manager

  use { "nvim-lualine/lualine.nvim", requires = { use 'kyazdani42/nvim-web-devicons', opt = true } }

  -- Colorschemes
  -- use "ericbn/vim-solarized" --solarized
  use "lifepillar/vim-solarized8" --solarized8
  -- use "NLKNguyen/papercolor-theme" --PaperColor
  -- use "preservim/vim-colors-pencil" --pencil
  -- use "yuttie/hydrangea-vim" --hydrangea

  -- Autocomplete
  use "hrsh7th/nvim-cmp" -- The completion plugin
  use "hrsh7th/cmp-buffer" -- buffer completions
  use "hrsh7th/cmp-path" -- path completions
  use "saadparwaiz1/cmp_luasnip" -- snippet completions
  use "hrsh7th/cmp-nvim-lsp" -- lsp completions

  --snippets
  use "L3MON4D3/LuaSnip" --snippet engine
  use "rafamadriz/friendly-snippets" -- a bunch of snippets to use

  --LSP
  use "williamboman/mason.nvim" -- simple to use language server installer
  use "williamboman/mason-lspconfig.nvim" -- bridges mason and nvim-lspconfig 
  use "neovim/nvim-lspconfig" -- enable LSP
  use "jose-elias-alvarez/null-ls.nvim" -- formatters and linters
  use "jay-babu/mason-null-ls.nvim"

  --Fuzzy Finder
  use "nvim-telescope/telescope.nvim"

  --Syntax Highlight
  -- use "nathom/filetype.nvim"
  use { "nvim-treesitter/nvim-treesitter", run = ":TSUpdate", }
  use "lukas-reineke/indent-blankline.nvim"

  -- Git
  use "lewis6991/gitsigns.nvim"

  --File Explorer
  use { "kyazdani42/nvim-tree.lua", requires = { 'kyazdani42/nvim-web-devicons', } }
  use { 'akinsho/bufferline.nvim', tag = "*", requires = 'kyazdani42/nvim-web-devicons' }


  use "akinsho/toggleterm.nvim"
  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if packer_bootstrap then
    require('packer').sync()
  end
end)
