local ok, configs = pcall(require, "nvim-treesitter.configs")
if not ok then
  vim.notify("Error: plugin nvim-treesitter not found!")
  return
end

configs.setup({
  auto_install = true,
  highlight = {
    enable = true,
  },
  indent = {
    enable = true
  }
})
